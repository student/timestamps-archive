# timestamps-archive

Lists files to be archived (or cleaned up) in directory with timestamped files.  Daily/monthly/yearly archive filter for timestamped files. 


# Examples

```shell 
# list top 10 timestamped files in current directory
timestamps-archive.py --format "archive_%Y-%m-%d.zip"  --top 10

# first timestamped file from last 12 months  
timestamps-archive.py --format "%Y-%m-%d"  --months 12 

# delete timestamped files except latest 5 and last 10 years
timestamps-archive.py --format "%Y-%m-%d"  --top 5 --years 10 --invert --delete

# print help
timestamps-archive.py -h 
usage: timestamps-archive.py [-h] [--path PATH] [--format FORMAT] [--top TOP]
                             [--days DAYS] [--months MONTHS] [--years YEARS]
                             [--invert] [--delete]

Daily/monthly/yearly archive filter for timestamped files/directories

optional arguments:
  -h, --help       show this help message and exit
  --path PATH      files path, default is current directory
  --format FORMAT  date/time format, default is %Y-%m-%dT%H:%M:%S
  --top TOP        include TOP files
  --days DAYS      include number of DAYS
  --months MONTHS  include number of MONTHS
  --years YEARS    include number of YEARS
  --invert         invert filter
  --delete         delete filtered files/directories
```

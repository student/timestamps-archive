#!/bin/env python

# Lists files to be archived (or cleaned up) in directory with timestamped files.  Daily/monthly/yearly archive filter for timestamped files.

import os, itertools, argparse, datetime, shutil, stat

DATETIME_FORMAT='%Y-%m-%dT%H:%M:%S'

def main():
  
  parser = argparse.ArgumentParser(description='Daily/monthly/yearly archive filter for timestamped files/directories')
  parser.add_argument('--path', type=str, default='.', help=f'files path, default is current directory')
  parser.add_argument('--format', type=str, default=DATETIME_FORMAT,  help=f'date/time format, default is {DATETIME_FORMAT.replace("%","%%")}')
  parser.add_argument('--top', type=int, default=0, help='include TOP files')
  parser.add_argument('--days', type=int, default=0, help='include number of DAYS')
  parser.add_argument('--months', type=int, default=0, help='include number of MONTHS')
  parser.add_argument('--years', type=int, default=0, help='include number of YEARS')
  parser.add_argument('--invert', action='store_true', help='invert filter')
  parser.add_argument('--delete', action='store_true', help='delete filtered files/directories')
  
  args = parser.parse_args()
  
  names_dates = []
  for de in os.scandir(args.path):
    try:
      d = datetime.datetime.strptime(de.name, args.format)
    except ValueError:
      continue
      
    names_dates.append((de.name, d))
  
  names_dates.sort(key=lambda v: v[1], reverse=True)
  
  top = names_dates[:args.top]
  
  days = itertools.islice((list(v)[-1] for g, v in itertools.groupby(names_dates, lambda v: v[1].replace(hour=0, minute=0, second=0, microsecond=0))), args.days)

  months = itertools.islice((list(v)[-1] for g, v in itertools.groupby(names_dates, lambda v: v[1].replace(day=1, hour=0, minute=0, second=0, microsecond=0))), args.months)

  years = itertools.islice((list(v)[-1] for g, v in itertools.groupby(names_dates, lambda v: v[1].replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0))), args.years)

  include = list(itertools.chain(top, days, months, years))
  
  if args.invert:
    res = [v[0] for v in names_dates if v not in include]
  else:
    res = [v[0] for v in names_dates if v in include]

  # clear the readonly bit and reattempt the removal
  def remove_readonly(func, path, excinfo):
    os.chmod(path, stat.S_IWRITE)
    func(path)
  
  for l in res:
    print(l)
    if args.delete:
      path = os.path.join(args.path, l)
      if os.path.isdir(path):
        shutil.rmtree(path, onerror=remove_readonly)
      else:
        os.remove(path)

if __name__ == '__main__':
  main()
